; Convert a pattern address in ROM to the index among all patterns in VRAM
MPatternAddressToIndex: MACRO   PatternAddress,Dest
                        MOVE.W  #((VaPatterns + (\PatternAddress - Patterns)) / $20),\Dest
                        ENDM

; Convert a palette address in ROM to the index among all palettes in CRAM
MPaletteAddressToIndex: MACRO   PaletteAddress,Dest
                        MOVE.W  #((\PaletteAddress - Palettes) / $20),\Dest
                        ENDM
