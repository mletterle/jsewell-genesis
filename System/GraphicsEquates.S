; Graphics Equates
; ----------------
; General constants related to graphics values.

; Size of VRAM in bytes
LcVideoRamSizeBytes:        EQU $10000

; ***
; *** Colors
; ***

; Bytes to represent a color in CRAM
BcColorBytes:               EQU 2
; Colors per color line
BcColorLineColors:          EQU 16
; Bytes per color line in CRAM
BcColorLineBytes:           EQU BcColorBytes*BcColorLineColors
; Color lines in CRAM
BcColorLines:               EQU 4
; Size of CRAM in bytes
BcColorRamBytes:            EQU BcColorLines*BcColorLineBytes

; ***
; *** Pattern data
; ***

; Width of a pattern in pixels
BcPatternWidthPixels:       EQU 8
; Height of a pattern in pixels
BcPatternHeightPixels:      EQU 8
; Pixels per byte (not bytes/pixel) in a pattern
BcPatternPixelsPerByte:     EQU 2
; Size of pattern in bytes
BcPatternBytes:             EQU BcPatternWidthPixels*BcPatternHeightPixels/BcPatternPixelsPerByte

; ***
; *** Planes / Window
; ***

; Size of a nametable (tilemap) entry in bytes
BcNametableEntryBytes:      EQU 2
; Nametable entry construction
    ; Byte 0
        ; bit 7:    Priority
        ; bit 6-5:  Palette Line
        ; bit 4:    Flip vertically
        ; bit 3:    Flip horizontally
        ; bit 2-0:  Pattern index bits 10-8
    ; Byte 1:       Pattern index bits 7-0 
    ;               Pattern index = VRAM address / $20.
; Amount to left-shift the palette line number into a nametable entry
BcNametablePaletteLShift:   EQU 13
; Flag to treat a character as high-priority in the plane
WcNametablePriority:        EQU (1 << 15)
; Flag to flip the character vertically in the plane
WcNametableVFlip:           EQU (1 << 12)
; Flag to flip the character horizontally in the plane
WcNametableHFlip:           EQU (1 << 11)

; Width of the Window nametable in cells
BcWindowWidthCells:         EQU 32
; Height of the Window nametable in cells
BcWindowHeightCells:        EQU 32
; Size of the Window nametable in bytes
WcWindowNametableBytes:     EQU BcWindowWidthCells*BcWindowHeightCells*BcNametableEntryBytes

; Number of scrollable planes
BcScrollPlanes:             EQU 2
; Size of a single scroll value in bytes
BcScrollEntryBytes:         EQU 2
; Size of a collection of scroll values, one for each scrollable plane
BcScrollPairBytes:          EQU BcScrollPlanes*BcScrollEntryBytes
; Maximum pairs of scroll values in Vertical Scroll RAM, (2-cell wide scroll, 40-cell width)
BcVScrollMaxPairs:          EQU 20
; Size of VSRAM in bytes
BcVScrollRamSizeBytes:      EQU BcVScrollMaxPairs*BcScrollPairBytes

; ***
; *** Sprites
; ***

; Size of a sprite table/list entry in bytes
BcSpriteTableEntryBytes:        EQU 8
BcSpriteTableEntryBytesShift:   EQU 3
; Sprite table entry construction
    ; Byte 0 (Word 0 MSB)
        ; bit 7-2:  0
        ; bit 1-0:  Vertical position bits 9-8
    ; Byte 1 (Word 0 LSB)
        ; bit 7-0:  Vertical position bits 7-0
    ; Byte 2 (Word 1 MSB)
        ; bit 7-4:  0
        ; bit 3-2:  Horizontal size
        ;       00: 1 cell
        ;       01: 2 cells
        ;       10: 3 cells
        ;       11: 4 cells
        ; bit 1-0:  Vertical size
        ;       00: 1 cell
        ;       01: 2 cells
        ;       10: 3 cells
        ;       11: 4 cells
    ; Byte 3 (Word 1 LSB)
        ; bit 7:    0
        ; bit 6-0:  Next entry index
    ; Byte 4 (Word 2 MSB)
        ; bit 7:    Priority
        ; bit 6-5:  Palette Line
        ; bit 4:    Flip vertically
        ; bit 3:    Flip horizontally
        ; bit 2-0:  Pattern index bits 10-8
    ; Byte 5 (Word 2 LSB)
        ; bit 7-0:  Pattern index bits 7-0
    ; Byte 6 (Word 3 MSB)
        ; bit 7-1:  0
        ; bit 0:    Horizontal position bit 8
    ; Byte 7 (Word 3 LSB)
        ; bit 7-0:  Horizontal position bits 7-0
    ; Pattern Index
        ; Pattern index = VRAM address / $20.
        ;
        ; In multi-cell sprites, this index points to the pattern
        ; used in the upper left corner. The next patterns in VRAM
        ; are read for other cells, proceeding DOWN FIRST, then
        ; moving to the next column.
        ;
        ; So with the largest sprite size (4x4 cells) pointing
        ; at pattern index 0, the sprite looks like:
        ;
        ;       048C
        ;       159D
        ;       26AE
        ;       37BF
    ; Position
        ; In Progressive or Interlaced 1 scan modes, the sprites can 
        ; be assigned any position in 512x512. The visible screen's
        ; top left corner is coordinate 128,128. The other corners
        ; depend on the Width and Height settings.
        ;
        ; In Interlaced 2 scan mode, the vertical coordinates are
        ; doubled (which is why there's one more bit for vertical
        ; position than horizontal position).
        ;
        ; Try not to place sprites on x coordinate 0, as this can
        ; cause masking to happen for other sprites on that y coordinate.
    ; Entry Indices
        ; The sprite table is indexed like an array but read
        ; like a linked list! The index of a sprite is its offset
        ; from the start of the table, divided by 8 (the size of each
        ; entry). The VDP reads the sprites starting at the base position
        ; of the table (so entry #0), but then follows the "next entry
        ; index" field of the sprite it just read to get to the next
        ; sprite. This continues until it finds a "next entry index"
        ; of 0.
WcSpriteWidth1:     EQU (%00 << 10)
WcSpriteWidth2:     EQU (%01 << 10)
WcSpriteWidth3:     EQU (%10 << 10)
WcSpriteWidth4:     EQU (%11 << 10)
WcSpriteHeight1:    EQU (%00 << 8)
WcSpriteHeight2:    EQU (%01 << 8)
WcSpriteHeight3:    EQU (%10 << 8)
WcSpriteHeight4:    EQU (%11 << 8)