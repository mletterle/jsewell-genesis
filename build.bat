@echo off
setlocal

rem Source file.
rem The file containing the assembly code. This file includes the content of multiple other files.
set source=CodeMash.S

rem Binary file (ROM image).
rem The file to produce containing the assembled binary. Put this in your emulator or flashcart.
set binary=CodeMash.BIN

rem No-checksum binary file
set nochecksum=CodeMash.BAK.BIN

rem Listing file.
rem This is your assembly code (with all the includes resolved) annotated with the bytes that the assembler 
rem emitted for each line. Useful for knowing where to place breakpoints in a 68K debugger.
set list=CodeMash.L68

rem Tool to translate a text file containing characters to an equivalent binary file.
set TextRectangleToBinary=Tools\TextRectangleToBinary\bin\Debug\TextRectangleToBinary.exe

rem Tool to translate a Tile Layer Pro palette file to an equivalent Genesis-format binary.
set PaletteConverter=Tools\PaletteConverter\bin\Debug\PaletteConverter.exe

rem Tool to calculate and set the checksum in a Sega Genesis ROM.
set ChecksumFixer=Tools\ChecksumFixer\bin\Debug\ChecksumFixer.exe

if not [%CD%\]==[%~dp0] (
    echo You need to run with this script's directory as the working directory.
    exit /b 1
)

rem ---------------------------------------
echo # Clean

echo ## Removing Level Data Binary Files...
for %%f in (LevelData\*.BIN) do (
    echo ### %%f
    del %%f
    if %errorlevel% neq 0 goto :error
)

echo ## Removing Genesis-format Palette Files...
for %%f in (Palettes\*.BIN) do (
    echo ### %%f
    del %%f
    if %errorlevel% neq 0 goto :error
)

echo ## Removing Z80 Machine Code Files...
for %%f in (Z80\*.BIN) do (
    echo ### %%f
    del %%f
    if %errorlevel% neq 0 goto :error
)

if exist %binary% (
    echo ## Removing 68k Machine Code File...
    echo ### %binary%
    del %binary%
    if %errorlevel% neq 0 goto :error
)

if exist %list% (
    echo ## Removing 68k Listing File...
    echo ### %list%
    del %list%
    if %errorlevel% neq 0 goto :error
)

if "%1"=="--clean" (
    echo --- Clean OK at %time% ---
    exit /b 0
)

rem ---------------------------------------
echo # Build

echo ## Generating Level Data Binary Files from Text...
for %%f in (LevelData\*.TXT) do (
    echo ### LevelData\%%~nf.TXT -> LevelData\%%~nf.BIN
    %TextRectangleToBinary% --in LevelData\%%~nf.TXT --out LevelData\%%~nf.BIN -w 128 -h 32 -m "-,FF" -m "T,66"
    if %errorlevel% neq 0 goto :error
)

echo ## Generating Genesis-format Palette Files from Tile Layer Pro Palettes...
for %%f in (Palettes\*.TPL) do (
    echo ### Palettes\%%~nf.TPL -> Palettes\%%~nf.TPL.BIN
    %PaletteConverter% Palettes\%%~nf.TPL
    if %errorlevel% neq 0 goto :error
)

echo ## Assembling Z80 Machine Code from Z80 Assembly...
for %%f in (Z80\*.ASM) do (
    echo ### Z80\%%~nf -> Z80\%%~nf.BIN
    yaza Z80\%%~nf.ASM --output:Z80\%%~nf.BIN
    if %errorlevel% neq 0 goto :error
)

echo ## Assembling 68k Machine Code (ROM image) from 68k Assembly...
echo ### %source% -> %binary% ; %list%
asm68k /o ws+ /m /p %source%,%binary%,NUL,%list%
rem /o ws+ allows whitespace in expressions
rem /m includes macros in the list file
if %errorlevel% neq 0 goto :error

echo ## Setting Checksum for ROM image...
%ChecksumFixer% %binary%
if %errorlevel% neq 0 goto :error
del %nochecksum%

echo --- Build OK at %time% ---
exit /b 0

:error
echo --- Build FAILED at %time% ---
exit /b 1