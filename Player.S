***
*** Player Character
***

; The player character is controlled by the player. This file handles input
; and gravity, friction, etc. to control the player character's position and
; velocity. The player character's position affects the camera and the sprite
; representing the player character, which are both in separate files.

** Variables **

; Player character position within the level. Expressed as the bottom center
; of the sprite, in subpixels.
WaPlayerPosX:       RS.W    1
WaPlayerPosY:       RS.W    1

; Player velocity (change in subpixels/frame)
WaPlayerVelX:       RS.W    1
WaPlayerVelY:       RS.W    1

; Player state
BaPlayerState:      RS.B    1
BcPlayerState_Falling:  EQU     0   ; airborne, experiencing normal gravity
BcPlayerState_Jumping:  EQU     1   ; airborne, experiencing jump gravity*
BcPlayerState_Ground:   EQU     2   ; grounded
    ; * Here's how jumps work. On the ground, pressing a jump button
    ;   will add upward velocity to the player and make the player airborne.
    ;   The player will experience reduced gravity until either they
    ;   start descending or the jump button is released. So you can
    ;   release the jump button early to control the jump trajectory in midair.

*** Physics Constants ***

; Max speed the player can move horizontally (subpixels per frame)
; in either direction, when on the ground.
WcGroundMaxVelX:            EQU     150
; Max speed the player can move upward (subpixels per frame).
WcMaxVelYUp:                EQU     256
; Max speed the player can move downward (subpixels per frame).
WcMaxVelYDown:              EQU     128

; Increase in horizontal speed as the D-pad is held in the direction of
; existing horizontal velocity (or from zero velocity) on the ground.
WcGroundInputAccelX:        EQU     24
; Decrease in horizontal speed as the D-pad is held in the opposite direction of
; existing horizontal velocity on the ground.
WcGroundInputDecelX:        EQU     18
; Decrease in horizontal speed if the D-pad is not held on the ground.
WcGroundFrictionDecelX:     EQU     10

; Increase in horizontal speed as the D-pad is held in the direction of
; existing horizontal velocity (or from zero velocity) in the air.
WcAirInputAccelX:           EQU     18
; Decrease in horizontal speed as the D-pad is held in the opposite direction of
; existing horizontal velocity in the air.
WcAirInputDecelX:           EQU     10
; Decrease in horizontal speed if the D-pad is not held in the air.
WcAirFrictionDecelX:        EQU     2

; Increase in downward vertical speed when airborne.
WcAirGravityY:              EQU     16
; Increase in downward vertical speed when airborne while the jump button
; is held during a jump.
WcAirGravityJumpY:          EQU     6
; Increase in upward vertical speed at the start of a jump.
WcJumpAccelY:               EQU     192

** X-direction acceleration arrays **

; There's an array for each combination of state [grounded, airborne] and
; current velocity [zero, positive, negative]. Each array contains three
; elements: velocity to add if the player makes no input, velocity to add if
; the player presses right (positive X direction), and velocity to add if
; the player presses left (negative X direction).

; On the ground, not moving
AccelGroundNoXVelocity:
        DC.W    0                       ; no input, stay still
        DC.W    WcGroundInputAccelX     ; input right, start accel right
        DC.W    -WcGroundInputAccelX    ; input left, start accel left
; On the ground, moving right
AccelGroundPositiveXVelocity:
        DC.W    -WcGroundFrictionDecelX ; no input, decel due to friction
        DC.W    WcGroundInputAccelX     ; input right, accel
        DC.W    -WcGroundInputDecelX    ; input left, decel faster than friction
                                        ; so we can move left sooner
; On the ground, moving left
AccelGroundNegativeXVelocity:
        DC.W    WcGroundFrictionDecelX  ; no input, decel due to friction
        DC.W    WcGroundInputDecelX     ; input right, decel faster than
                                        ; friction so we can move right sooner
        DC.W    -WcGroundInputAccelX    ; input left, accel
; Airborne, not moving horizontally
AccelAirNoXVelocity:
        DC.W    0                       ; no input, stay still
        DC.W    WcAirInputAccelX        ; input right, start accel right
        DC.W    -WcAirInputAccelX       ; input left, start accel left
; Airborne, moving right
AccelAirPositiveXVelocity:
        DC.W    -WcAirFrictionDecelX    ; no input, decel due to friction
        DC.W    WcAirInputAccelX        ; input right, accel
        DC.W    -WcAirInputDecelX       ; input left, decel faster than friction
                                        ; so we can move left sooner
; Airborne, moving left
AccelAirNegativeXVelocity:
        DC.W    WcAirFrictionDecelX     ; no input, decel due to friction
        DC.W    WcAirInputDecelX        ; input right, decel faster than
                                        ; friction so we can move right sooner
        DC.W    -WcAirInputAccelX       ; input left, accel

** Sprite Constants / Variables **

BcPlayerSpriteWidthCells:   EQU     2
BcPlayerSpriteHeightCells:  EQU     3
BcPlayerSpriteSizeSettings: EQU     WcSpriteWidth2|WcSpriteHeight3

BcPlayerWidthPixels:        EQU     BcPlayerSpriteWidthCells*BcPixelsInCell
BcPlayerHeightPixels:       EQU     BcPlayerSpriteHeightCells*BcPixelsInCell

; Sprite number of the player sprite in the sprite table
WaPlayerSpriteNum:          RS.W    1

** Code **

; Initializes player character.
RInitPlayer:
    ; Start at the top of the level at x position 0
        MOVE.W  #0,WaPlayerPosX
        MOVE.W  #0,WaPlayerPosY

    ; Start with no velocity
        MOVE.W  #0,WaPlayerVelX
        MOVE.W  #0,WaPlayerVelY

    ; Start airborne, not in a jump
        MOVE.B  #BcPlayerState_Falling,BaPlayerState

    ; Initialize sprite
        MOVE.W  #BcPlayerSpriteSizeSettings,-(SP)
        MOVE.L  #PatternDigits,-(SP)
        MOVE.L  #PaletteText,-(SP)
        JSR     RAddSprite
        MOVE.W  D7,WaPlayerSpriteNum
        ADD.L   #2+4+4,SP   ; pop arguments

        RTS

; Updates the player character (as part of the main routine).
RUpdatePlayer:

    ; Use registers for speed/convenience
    @VelX:      EQUR    D0
    @VelY:      EQUR    D1
    @Collision: EQUR    D7
    @XTable:    EQUR    A0
    
    ; Macro for getting collision into D7
    @MGetCollision:     MACRO
        ; push registers onto the stack that we or the subroutine use
        MOVEM.L D5-D6/A0,-(SP)

        ; push stack arguments
        MOVE.W  (WaPlayerPosX),D5       ; D5 = player X position in subpixels
        MOVE.W  D5,-(SP)                ; push X onto stack as argument

        MOVE.W  (WaPlayerPosY),D5       ; D5 = player Y position in subpixels
        MOVE.W  D5,-(SP)                ; push Y onto stack as argument

        ; call
        JSR     RGetCellKind

        ; pop stack arguments
        ADD.L   #4,SP

        ; pop registers
        MOVEM.L (SP)+,D5-D6/A0
                        ENDM

    ; Load velocity
        MOVE.W  (WaPlayerVelX),@VelX
        MOVE.W  (WaPlayerVelY),@VelY

    ; Are we grounded?
        CMP.B   #BcPlayerState_Ground,BaPlayerState
        BEQ     @Grounded

    ; Airborne
    @Airborne:
        ; Check for collision with platform
    @Airborne_CheckCollision:
        @MGetCollision
        CMP.B   #BcCellKindPlatform,@Collision
        BEQ     @Airborne_CollidePlatform
        JMP     @Airborne_CollisionDone
    @Airborne_CollidePlatform:
        MOVE.B  #BcPlayerState_Ground,BaPlayerState
        ADD.W   #-1,(WaPlayerPosY)          ; move upward 1 unit
        JMP     @Airborne_CheckCollision    ; then recheck collision
    @Airborne_CollisionDone:

        ; Switch on state to apply gravity / jump logic
        MOVE.B  BaPlayerState,D2
        CMP.B   #BcPlayerState_Ground,D2
        BEQ     @Grounded_ChooseTable       ; Possible if we collided with
                                            ; a platform this frame.
                                            ; Ignore the rest of airborne
                                            ; section for this frame,
                                            ; but get X velocity values for
                                            ; the grounded state.
        CMP.B   #BcPlayerState_Falling,D2
        BEQ     @Airborne_GravityFalling
        CMP.B   #BcPlayerState_Jumping,D2
        BEQ     @Airborne_GravityJumping
        TRAP    #1  ; Unrecognized player state
    @Airborne_GravityJumping:
        ; Check if we're no longer jumping, defined as having a downward
        ; velocity or having released the jump button.
        CMP.W   #0,@VelY
        BGT     @Airborne_GravityFalling
        MOVE.W  (WaJoypad1),D2
        AND.W   #WcJoypadA|WcJoypadB|WcJoypadC,D2
        BEQ     @Airborne_GravityFalling
        ADD.W   #WcAirGravityJumpY,@VelY
        JMP     @Airborne_GravityDone
    @Airborne_GravityFalling:
        MOVE.B  #BcPlayerState_Falling,BaPlayerState
        ADD.W   #WcAirGravityY,@VelY
    @Airborne_GravityDone:

        ; Clamp vertical velocity
        CMP.W   #WcMaxVelYDown,@VelY
        BLE     @Airborne_VelYClampedDown
        MOVE.W  #WcMaxVelYDown,@VelY
    @Airborne_VelYClampedDown:
        CMP.W   #-WcMaxVelYUp,@VelY
        BGT     @Airborne_VelYClamped
        MOVE.W  #-WcMaxVelYUp,@VelY
    @Airborne_VelYClamped:

        ; Switch XTable based on current X velocity
    @Airborne_ChooseTable:
        CMP.W   #0,@VelX
        BEQ     @Airborne_NoX           ; VelX == 0
        BGT     @Airborne_PosX          ; VelX > 0
        BLT     @Airborne_NegX          ; VelX < 0
        TRAP    #1  ; Unrecognized X-velocity
    @Airborne_NoX:
        LEA     (AccelAirNoXVelocity),@XTable
        JMP     @Both
    @Airborne_PosX:
        LEA     (AccelAirPositiveXVelocity),@XTable
        JMP     @Both
    @Airborne_NegX:
        LEA     (AccelAirNegativeXVelocity),@XTable
        JMP     @Both

    ; Grounded
    @Grounded:
        ; Check for jump input
        MOVE.W  (WaJoypad1),D2
        AND.W   #WcJoypadA|WcJoypadB|WcJoypadC,D2
        BEQ     @Grounded_NoJump
        ; Start a jump
        MOVE.W  #-WcJumpAccelY,@VelY
        MOVE.B  #BcPlayerState_Jumping,BaPlayerState
        JMP     @Airborne_ChooseTable
    @Grounded_NoJump:
        ; No vertical movement when grounded
        MOVE.W  #0,@VelY
    @Grounded_AfterJump:

        ; Check for loss of floor collision
        @MGetCollision
        CMP.B   #BcCellKindPlatform,@Collision
        BEQ     @Grounded_CollisionDone
        MOVE.B  #BcPlayerState_Falling,(BaPlayerState)
    @Grounded_CollisionDone:

        ; Switch XTable based on current X velocity
    @Grounded_ChooseTable:
        CMP.W   #0,@VelX
        BEQ     @Grounded_NoX           ; VelX == 0
        BGT     @Grounded_PosX          ; VelX > 0
        BLT     @Grounded_NegX          ; VelX < 0
        TRAP    #1  ; Unrecognized X-velocity
    @Grounded_NoX:
        LEA     (AccelGroundNoXVelocity),@XTable
        JMP     @Both
    @Grounded_PosX:
        LEA     (AccelGroundPositiveXVelocity),@XTable
        JMP     @Both
    @Grounded_NegX:
        LEA     (AccelGroundNegativeXVelocity),@XTable
        JMP     @Both

    ; Both grounded and airborne
    @Both:
        ; Handle D-pad input
        MOVE.W  (WaJoypad1),D2
        AND.W   #WcJoypadRight|WcJoypadLeft,D2
        BEQ     @NoXInput   ; if neither left/right is held, no input
        CMP.W   #WcJoypadRight|WcJoypadLeft,D2
        BEQ     @NoXInput   ; if BOTH left/right are held, no input
        CMP.W   #WcJoypadRight,D2
        BEQ     @PosXInput
        CMP.W   #WcJoypadLeft,D2
        BEQ     @NegXInput
        TRAP    #1  ; Unrecognized D-pad X-input
    @NoXInput:
        ADD.W   (0,A0),@VelX
        ; TODO: If we switched signs, round down to 0
        JMP     @XInputHandled
    @PosXInput:
        ADD.W   (2,A0),@VelX
        JMP     @XInputHandled
    @NegXInput:
        ADD.W   (4,A0),@VelX
        JMP     @XInputHandled
    @XInputHandled:

        ; Clamp horizontal velocity
        CMP.W   #WcGroundMaxVelX,@VelX
        BLE     @VelXClampedRight
        MOVE.W  #WcGroundMaxVelX,@VelX
    @VelXClampedRight:
        CMP.W   #-WcGroundMaxVelX,@VelX
        BGT     @VelXClamped
        MOVE.W  #-WcGroundMaxVelX,@VelX
    @VelXClamped:

        ; Load position
        MOVE.W  (WaPlayerPosX),D2
        MOVE.W  (WaPlayerPosY),D3

        ; Apply velocity to position
        ADD.W   @VelX,D2
        ADD.W   @VelY,D3

        ; Clamp position to level boundaries
        AND.W   #(((BcLevelWidthCells*8)-1)<<BcSubpixelsInPixelShift)|(BcSubpixelsInPixel-1),D2
        AND.W   #(((BcLevelHeightCells*8)-1)<<BcSubpixelsInPixelShift)|(BcSubpixelsInPixel-1),D3

        ; Set position of sprite
        MOVE.W  WaPlayerSpriteNum,-(SP) ; entry number
        MOVE.W  D3,-(SP)                ; y pos
        MOVE.W  D2,-(SP)                ; x pos
        JSR     RSetSpritePosition
        ADD.L   #2+2+2,SP             ; pop arguments

        ; Store position and velocity
        MOVE.W  @VelX,(WaPlayerVelX)
        MOVE.W  @VelY,(WaPlayerVelY)
        MOVE.W  D2,(WaPlayerPosX)
        MOVE.W  D3,(WaPlayerPosY)

        RTS