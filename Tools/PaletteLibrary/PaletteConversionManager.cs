﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace PaletteLibrary
{
    public class PaletteConversionManager
    {
        private static Dictionary<GenesisPaletteComponent, byte> tlpByteToGenesisComponent =
            new Dictionary<GenesisPaletteComponent, byte>
            {
                { GenesisPaletteComponent.Val0, 0 },
                { GenesisPaletteComponent.Val2, 52 },
                { GenesisPaletteComponent.Val4, 87 },
                { GenesisPaletteComponent.Val6, 116 },
                { GenesisPaletteComponent.Val8, 144 },
                { GenesisPaletteComponent.ValA, 172 },
                { GenesisPaletteComponent.ValC, 206 },
                { GenesisPaletteComponent.ValE, 255 },
            };

        /// <summary>
        /// Converts a TLP color to the equivalent (or nearest) Genesis color.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public GenesisPaletteEntry TlpToGenesisColor(TileLayerPaletteEntry input)
        {
            return new GenesisPaletteEntry
            {
                Red = TlpToGenesisComponent(input.Red),
                Green = TlpToGenesisComponent(input.Green),
                Blue = TlpToGenesisComponent(input.Blue),
            };
        }

        public GenesisPaletteComponent TlpToGenesisComponent(byte input)
        {
            GenesisPaletteComponent closest = null;
            var closestDiff = Int32.MaxValue;

            foreach (var kvp in tlpByteToGenesisComponent)
            {
                var diff = Math.Abs(input - (int) kvp.Value);
                if (diff < closestDiff)
                {
                    closest = kvp.Key;
                    closestDiff = diff;
                }
            }

            if (closest == null)
            {
                throw new ApplicationException("Somehow couldn't find any closest match...");
            }

            return closest;
        }
    }
}
