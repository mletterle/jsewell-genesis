﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaletteLibrary
{
    /// <summary>
    /// Represents a Tile Layer (Pro) palette entry, consisting of 8-bit red, green, and blue components.
    /// </summary>
    public class TileLayerPaletteEntry
    {
        public byte Red { get; set; }
        public byte Green { get; set; }
        public byte Blue { get; set; }

        public override string ToString()
        {
            return $"{nameof(TileLayerPaletteEntry)} {Red:X2} {Green:X2} {Blue:X2}";
        }
    }
}
