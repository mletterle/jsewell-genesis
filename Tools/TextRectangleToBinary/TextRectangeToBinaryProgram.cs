﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Options;

namespace TextRectangleToBinary
{
    class TextRectangeToBinaryProgram
    {
        static int Main(string[] args)
        {
            try
            {
                // Parse options
                string inFilename = null;
                string outFilename = null;
                int? width = null;
                int? height = null;
                Dictionary<char, byte> mapping = new Dictionary<char, byte>
                {
                    { ' ', 0x00 }
                };

                var options = new OptionSet
                {
                    {
                        "in=",
                        "text file to process [REQUIRED]",
                        x => inFilename = x
                    },
                    {
                        "out=",
                        "binary file to write [REQUIRED]",
                        x => outFilename = x
                    },
                    {
                        "w|width=",
                        "width (decimal) in characters (if unset, inferred from file)",
                        x => width = int.Parse(x)
                    },
                    {
                        "wx=",
                        "width (hexadecimal) in characters (if unset, inferred from file)",
                        x => width = int.Parse(x, System.Globalization.NumberStyles.HexNumber)
                    },
                    {
                        "h|height=",
                        "height (decimal) in characters (if unset, inferred from file)",
                        x => height = int.Parse(x)
                    },
                    {
                        "hx=",
                        "height (hexadecimal) in characters (if unset, inferred from file)",
                        x => height = int.Parse(x, System.Globalization.NumberStyles.HexNumber)
                    },
                    {
                        "m|map=",
                        "character,hexByte (can be specified multiple times)",
                        x =>
                        {
                            var splits = x.Split(',');
                            if (splits.Length != 2)
                            {
                                throw new ApplicationException("mapping must be specified as character,hexByte e.g. _,7F");
                            }

                            var charString = splits[0];
                            if (charString.Length != 1)
                            {
                                throw new ApplicationException("mapping must be specified as character,hexByte e.g. _,7F");
                            }
                            var mapChar = charString.Single();

                            var byteString = splits[1];
                            var mapByte = byte.Parse(byteString, System.Globalization.NumberStyles.HexNumber);

                            mapping[mapChar] = mapByte;
                        }
                    }
                };

                var leftover = options.Parse(args);

                if (args.Length == 0 || leftover.Any())
                {
                    Console.WriteLine("Description: \n\tConverts a text file, describing a rectangular array, into a binary file.");
                    Console.WriteLine("Options:");
                    options.WriteOptionDescriptions(Console.Out);
                    return 2;
                }
                if (inFilename == null)
                {
                    throw new ApplicationException("Must specify input filename");
                }
                if (outFilename == null)
                {
                    throw new ApplicationException("Must specify output filename");
                }

                // Log options
                void LogNullableInt(string name, int? value)
                {
                    if (value == null)
                    {
                        Console.WriteLine($"{name}: will be inferred from input");
                    }
                    else
                    {
                        Console.WriteLine($"{name}: {value} / 0x{value:X2}");
                    }
                }

                Console.WriteLine($"Input filename: {inFilename}");
                Console.WriteLine($"Output filename: {outFilename}");
                LogNullableInt("Width", width);
                LogNullableInt("Height", height);
                Console.WriteLine($"Byte mappings:");
                foreach (var map in mapping)
                {
                    Console.WriteLine($"  '{map.Key}' = 0x{map.Value:X2}");
                }
                Console.WriteLine();

                // Load file
                Console.WriteLine($"Reading '{inFilename}'...");
                var lines = File.ReadAllLines(inFilename);
                if (width == null)
                {
                    width = lines.Max(line => line.Length);
                    Console.WriteLine($"Width inferred as {width}");
                }
                if (height == null)
                {
                    height = lines.Length;
                    Console.WriteLine($"Height inferred as {height}");
                }

                // Create and fill array - [line, column]
                byte[,] array = new byte[height.Value,width.Value];
                for (int y = 0; y < lines.Length; y++)
                {
                    var line = lines[y];
                    for (int x = 0; x < line.Length; x++)
                    {
                        var c = line[x];
                        if (!mapping.ContainsKey(c))
                        {
                            throw new ApplicationException($"Character '{c}' has no mapping");
                        }
                        array[y, x] = mapping[c];
                    }
                }

                // Write out array
                Console.WriteLine($"Writing '{outFilename}...");
                var outArray = new byte[height.Value * width.Value];
                Buffer.BlockCopy(array, 0, outArray, 0, outArray.Length);
                File.WriteAllBytes(outFilename, outArray);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error: {ex}");
                return 1;
            }

            return 0;
        }
    }
}
