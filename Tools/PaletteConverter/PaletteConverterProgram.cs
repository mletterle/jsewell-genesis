﻿using PaletteLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaletteConverter
{
    internal static class PaletteConverterProgram
    {
        static byte ReadByteOrThrow(this FileStream fs, string expecting)
        {
            var read = fs.ReadByte();
            if (read == -1)
            {
                throw new ApplicationException($"Hit end of stream when reading {expecting}");
            }
            return (byte) read;
        }

        static int Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Description: \n\tConverts a Tile Layer (Pro) palette file to a binary format importable by Genesis assembly code.");
                Console.WriteLine("Usage: \n\tPaletteConverter Input.TPL");
                return 2;
            }

            try
            {
                var inputFilename = args[0];
                var outputFilename = Path.ChangeExtension(inputFilename, ".TPL.BIN");
                Console.WriteLine($"Input: {inputFilename}");
                Console.WriteLine($"Output: {outputFilename}");

                const int numberOfColors = 16;
                var tlp = new TileLayerPaletteEntry[numberOfColors];
                var gen = new GenesisPaletteEntry[numberOfColors];

                using (var fs = new FileStream(inputFilename, FileMode.Open, FileAccess.Read))
                {
                    // TPL format is the letters "T", "P", and "L", followed by a zero byte (for RGB format)
                    // then a continuing set of byte triads in order R, G, and B.
                    var expectedHeader = new byte[] { (byte) 'T', (byte) 'P', (byte) 'L' };
                    for (int i = 0; i < expectedHeader.Length; i++)
                    {
                        var expectedByte = expectedHeader[i];
                        var actualByte = fs.ReadByteOrThrow($"header byte #{i}");
                        if (expectedByte != actualByte)
                        {
                            throw new ApplicationException("This doesn't look like a Tile Layer Pro palette file (header mismatch)");
                        }
                    }

                    var paletteFormat = fs.ReadByteOrThrow("palette format byte");
                    if (paletteFormat != 0)
                    {
                        throw new ApplicationException(
                            "This doesn't look like an RGB-format Tile Layer Pro palette file;"
                            + " load it in TLP and select Palette -> Format -> RGB");
                    }

                    for (int i = 0; i < numberOfColors; i++)
                    {
                        tlp[i] = new TileLayerPaletteEntry
                        {
                            Red = fs.ReadByteOrThrow($"color {i}, red byte"),
                            Green = fs.ReadByteOrThrow($"color {i}, green byte"),
                            Blue = fs.ReadByteOrThrow($"color {i}, blue byte")
                        };
                    }

                    if (fs.Position != fs.Length)
                    {
                        throw new ApplicationException($"It looks like there's more than {numberOfColors} colors, which is more than the Genesis supports;" +
                            $" load the palette in TLP and select Palette -> Entries -> 16");
                    }
                }

                var manager = new PaletteConversionManager();
                for (int i = 0; i < numberOfColors; i++)
                {
                    gen[i] = manager.TlpToGenesisColor(tlp[i]);
                }

                using (var fs = new FileStream(outputFilename, FileMode.Create, FileAccess.Write))
                {
                    for (int i = 0; i < numberOfColors; i++)
                    {
                        foreach (var b in gen[i].ToBytes())
                        {
                            fs.WriteByte(b);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error: {ex}");
                return 1;
            }

            return 0;
        }
    }
}
